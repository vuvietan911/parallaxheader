//
//  AppDelegate.swift
//  CoverParallax
//
//  Created by Diep Nguyen Hoang on 7/11/15.
//  Copyright (c) 2015 CodenTrick. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .lightContent
        return true
    }
}

