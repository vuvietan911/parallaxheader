//
//  ViewController.swift
//  CoverParallax
//
//  Created by Diep Nguyen Hoang on 7/11/15.
//  Copyright (c) 2015 CodenTrick. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tbvMain: UITableView!
    @IBOutlet weak var redViewTopConstrain: NSLayoutConstraint!
    @IBOutlet weak var cstrBtnTopSpace: NSLayoutConstraint!

    @IBOutlet var cstrIVTopTLOV: NSLayoutConstraint!
    @IBOutlet var cstrIVTopScrollview: NSLayoutConstraint!
    @IBOutlet var cstrIVBottomBtn: NSLayoutConstraint!
    @IBOutlet var cstrIVHeight: NSLayoutConstraint!
    
        
    @IBOutlet weak var cstrBtnTopSpaceIV: NSLayoutConstraint!
    
    
    @IBOutlet weak var cstrTbvHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cstrIVTopScrollview.isActive = false
        cstrIVTopTLOV.isActive = true
        cstrIVBottomBtn.isActive = true
        cstrIVHeight.isActive = false
        
        
        
        setLeftBarBtn(image: #imageLiteral(resourceName: "page_back_arrow_icon.png"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setTransparentNavBar()
    }
    

    
    
    func setTransparentNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func setLeftBarBtn(image: UIImage){
        let btn = UIButton(type: .custom)
        btn.setImage(image, for: .normal)
        btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn.addTarget(self, action: #selector(clickBack), for: UIControlEvents.touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
    }
    
    func clickBack(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cstrTbvHeight.constant = tbvMain.contentSize.height
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
       
        
        cell.textLabel?.text = "\(indexPath.row)"
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset)
        if scrollView.contentOffset.y <= 0{
            
        }else{
            
        }
        
        if scrollView.contentOffset.y > 218{
            lblTitle.isHidden = true
            title = "TITLE"
            navigationController?.navigationBar.barTintColor = .white
            navigationController?.navigationBar.isTranslucent = false
            setLeftBarBtn(image: #imageLiteral(resourceName: "backArrow.png"))
        }else{
            lblTitle.isHidden = false
            title = ""
            navigationController?.navigationBar.barTintColor = .clear
            navigationController?.navigationBar.isTranslucent = true
            setLeftBarBtn(image: #imageLiteral(resourceName: "page_back_arrow_icon.png"))
        }
        
        if scrollView.contentOffset.y > 218{
            print("cross")
            cstrIVTopTLOV.isActive = false
            cstrIVBottomBtn.isActive = false
            cstrIVTopScrollview.isActive = true
            cstrIVHeight.isActive = true
            cstrBtnTopSpace.constant = scrollView.contentOffset.y + 64
            
            
            
        }else{
            print("not cross")
            cstrIVTopTLOV.isActive = true
            cstrIVBottomBtn.isActive = true
            cstrIVTopScrollview.isActive = false
            cstrIVHeight.isActive = false
            cstrBtnTopSpace.constant = 283
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            
            
//            navigationController?.navigationBar.tintColor = .clear
//            navigationController?.navigationBar.isTranslucent = false
        }
    }
    
}

